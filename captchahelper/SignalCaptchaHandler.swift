//
//  SignalCaptchaHandler.swift
//  captchahelper
//
//  Created by Shaun on 11/15/21.
//

import Foundation
import WebKit


class SignalCaptchaHandler : NSObject,  WKURLSchemeHandler {
    
    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
      //Check that the url path is of interest for you, etc...
        let urlstring = urlSchemeTask.request.url!.absoluteString
        print("Function: \(#function), line: \(#line)")
        print("==> \(urlSchemeTask.request.url?.absoluteString ?? "")\n")
        let data = urlstring.data(using: .utf8)
        
      //Create a NSURLResponse with the correct mimetype.
        let urlResponse = URLResponse(url: urlSchemeTask.request.url!, mimeType: "text/plain",
                                                 expectedContentLength: -1, textEncodingName: nil)
        urlSchemeTask.didReceive(urlResponse)
        urlSchemeTask.didReceive(data!)
        //IMPORTANT: Tell the task that you're done.
     urlSchemeTask.didFinish()
    }
    
    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {

    }
}

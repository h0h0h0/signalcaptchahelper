//
//  ViewController.swift
//  captchahelper
//
//  Created by Shaun on 11/14/21.
//

import Cocoa
import WebKit


class ViewController: NSViewController, WKUIDelegate, WKNavigationDelegate {

    var webView: WKWebView!
    var wvConfig: WKWebViewConfiguration!
    let DEFAULT_URL = "https://signalcaptchas.org/registration/generate.html"
    
    override func loadView() {
        super.loadView()
        self.wvConfig = WKWebViewConfiguration()
        self.wvConfig.setURLSchemeHandler(SignalCaptchaHandler(), forURLScheme: "signalcaptcha")
        self.webView = WKWebView(frame: CGRect(x: 0, y: 0, width: 450, height: 600),
                                 configuration: self.wvConfig)
        
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(webView)
       
        let url = URL(string: DEFAULT_URL)
        let request = URLRequest(url: url!)
        webView.load(request)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}


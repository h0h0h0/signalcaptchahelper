# Signal Captcha Helper - Swift 
## Helpful utility to get signal captcha codes 

Signal's captcha system is designed to be used in an embedded browser. This utility provides that and the bare minimum to parse the resulting token. This is a Swift clone of [signald captcha-helper](https://gitlab.com/signald/captcha-helper).

# building
clone the project and run in xcode. 

# using
Solve the presented CAPTCHA. The screen will forward to the CAPTCHA link. Copy the code after the signalcaptcha:// protocol. Use in your soups and sauces.

# license 
MIT

